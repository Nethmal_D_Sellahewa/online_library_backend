<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\API\V1\APIBaseController;
use App\Entities\Books\BooksRepository;
use App\Entities\Books\Book;
use EMedia\Api\Docs\APICall;
use Illuminate\Http\Request;

class BooksAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(BooksRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
						->setGroup('Books')
						->setName('All Books')
                	    ->setParams([
                	        'q|Search query',
                	        'page|Page number',
                	        'per_page|Per Page Count',
                        ])
                        ->setSuccessPaginatedObject(Book::class);
                });

		// $items = $this->repo->search()->paginate((int) $request->query('per_page',20));
		$items = Book::paginate((int) $request->query('per_page',20));

		return response()->apiSuccessPaginated($items);
	}

}
