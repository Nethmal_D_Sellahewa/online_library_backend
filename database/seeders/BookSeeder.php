<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use ElegantMedia\OxygenFoundation\Database\Seeders\SeedWithoutDuplicates;
use App\Entities\Books\Book;

class BookSeeder extends Seeder
{
    use SeedWithoutDuplicates;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = [
			[
				'name'	 => 'Harry Potter',
				'author' => 'JK',
				'isbn' => 123,
				'published_year' => 2001,
			],
			[
				'name'	 => 'White Rebbit',
				'author' => 'Not me xD',
				'isbn' => 1233,
				'published_year' => 2002,
			],
			[
				'name'	 => 'Art of War',
				'author' => 'Sun Tzu',
				'isbn' => 123123,
				'published_year' => 2007,
			],
			[
				'name'	 => 'Oder of the phoneix',
				'author' => 'JK',
				'isbn' => 12312,
				'published_year' => 2004,
			],
		];

		$this->seedWithoutDuplicates($books, Book::class,'name','author','isbn','published_year','name','author','isbn','published_year');
    }
}
